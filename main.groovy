class Scratchcards {
    def inputfile = new File("./input.txt")

    static void main(String[] args) {
        def scratchcards = new Scratchcards()
        def input = scratchcards.readInput()

        def result = 0

        input.each { game ->
            result += scratchcards.calculateGamePoints(game.winningNumbers, game.scratchValues)
        }

        def resultPartTwo = scratchcards.calculateAmountOfScratchcardsVersionTwo(input)

        println("Result: ${result}")
        println("Result part two: ${resultPartTwo}")
    }

    def readInput() {
        def input = []
        inputfile.eachLine { line ->
            def gameIdAndValues = splitGameIdAndValues(line)

            def gameId = gameIdAndValues.gameId
            def values = gameIdAndValues.values         

            def winningNumbersAndScratchValues = splitWinningNumbersAndScratchValues(values)
            def winningNumbers = winningNumbersAndScratchValues.winningNumbers
            def scratchValues = winningNumbersAndScratchValues.scratchValues

            input.add([
                    gameId: gameId,
                    winningNumbers: winningNumbers,
                    scratchValues: scratchValues
            ])
        }

        return input
    }

    def splitGameIdAndValues(String line) {
        def split = line.split(":")
        def gameIdString = split[0].trim()
        line = split[1].trim()

        split = gameIdString.split(" ")
        def gameId = split[split.length - 1]

        return [
                gameId: gameId,
                values: line
        ]
    }

    def splitWinningNumbersAndScratchValues(String values) {
        def split = values.split("\\|")

        def winningNumbers = removeSpacesFromInsideTheList(
            split[0].trim().split(" ")
        ).collect { it.toInteger() }

        def scratchValues = removeSpacesFromInsideTheList(
            split[1].trim().split(" ")
        ).collect { it.toInteger() }

        return [
                winningNumbers: winningNumbers,
                scratchValues: scratchValues
        ]
    }

    def removeSpacesFromInsideTheList(String[] list) {
        def newList = []
        list.each { item ->
            if (item != "" && item != " ") {
                newList.add(item.trim())
            }
        }
        return newList
    }

    def calculateAmountOfWinningScratchValues(List<Integer> winningNumbers, List<Integer> scratchValues) {
        def amountOfWinningScratchValues = 0
        winningNumbers.each { winningNumber ->
            scratchValues.each { scratchValue ->
                if (winningNumber == scratchValue) {
                    amountOfWinningScratchValues++
                }
            }
        }
        return amountOfWinningScratchValues
    }

    def calculateGamePoints(List<Integer> winningNumbers, List<Integer> scratchValues) {
        def amountOfWinningScratchValues = calculateAmountOfWinningScratchValues(winningNumbers, scratchValues)
        def result = 0
        if (amountOfWinningScratchValues > 0) {
            result = 2 ** (amountOfWinningScratchValues - 1)
        }

        return result
    }

    def addGameCloneToInput(List input, String gameId, List<Integer> winningNumbers, List<Integer> scratchValues) {
        def newInput = []
        def addedOnce = false
        input.each { game ->
            newInput.add(game)
            if (game.gameId == gameId && !addedOnce) {
                addedOnce = true
                newInput.add([
                        gameId: gameId,
                        winningNumbers: winningNumbers,
                        scratchValues: scratchValues
                ])
            }
        }

        return newInput
    }

    def findGameById(List input, Integer gameId) {
        def gameFound = null
        input.each { game ->
            if (game.gameId.toInteger() == gameId) {
                gameFound = game
            }
        }
        return gameFound
    }

    def calculateAmountOfScratchcardsVersionTwo(List input) {
        def winningScratchvaluesList = []

        input.each { game-> 
            winningScratchvaluesList.add(
                calculateAmountOfWinningScratchValues(game.winningNumbers, game.scratchValues)
            )
        }

        def totalScratchcardsByGame = []
        def invertedIndex = winningScratchvaluesList.size() - 1
        while (invertedIndex >= 0) {
            def amountOfWinningScratchValues = winningScratchvaluesList[invertedIndex]
            
            def amountForThisGame = 1
            while (amountOfWinningScratchValues > 0) {
                def totalScratchcardsByGameIndex = totalScratchcardsByGame.size() - (winningScratchvaluesList.size() - invertedIndex)

                amountForThisGame += totalScratchcardsByGame[totalScratchcardsByGameIndex + amountOfWinningScratchValues]
                amountOfWinningScratchValues--
            }

            def newTotalScratchcardsByGame = []
            newTotalScratchcardsByGame.add(amountForThisGame)
            totalScratchcardsByGame.each { totalScratchcards ->
                newTotalScratchcardsByGame.add(totalScratchcards)
            }
            totalScratchcardsByGame = newTotalScratchcardsByGame

            invertedIndex--
        }

        def result = 0
        totalScratchcardsByGame.each { totalScratchcards ->
            result += totalScratchcards
        }

        return result
    }

    /**
    * @deprecated This method is slow and should not be used
    * I mean it when I say it is slow, it does not finish in a foreseeable time
    * I left it here because it was the first solution I came up with and I wanted to keep it
    */
    def calculateAmountOfScratchcards(List input) {
        int totalScratchcards = 0
        while (input.size() > 0) {
            def game = input[0]
            input.remove(0)

            def gameId = game.gameId.toInteger()

            def amountOfWinningScratchValues = calculateAmountOfWinningScratchValues(game.winningNumbers, game.scratchValues)

            while (amountOfWinningScratchValues > 0) {
                gameId++
                def gameClone = findGameById(input, gameId)
                if (gameClone != null) {
                    input = addGameCloneToInput(input, gameClone.gameId, gameClone.winningNumbers, gameClone.scratchValues)
                }
                amountOfWinningScratchValues--
            }

            totalScratchcards++
        }

        return totalScratchcards
    }
}